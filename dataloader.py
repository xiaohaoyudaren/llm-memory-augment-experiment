from abc import ABC, abstractmethod
import json

# 基类数据加载器，用于加载数据集
class BaseDataLoader(ABC):
    def __init__(self, dataset):
        self.dataset_path = dataset
        self.data = None
        self.load_data()

    # 加载数据集到缓存中
    @abstractmethod
    def load_data(self):
        pass

    # 获取数据集规模
    @abstractmethod
    def get_size(self):
        pass

    # 获取下一个batch的数据
    @abstractmethod
    def get_next(self):
        pass

    @abstractmethod
    def has_next(self):
        pass

# GVD训练集
class GVDTrainDataLoader(BaseDataLoader):
    def __init__(self, dataset):
        super().__init__(dataset)

    def load_data(self):
        with open(self.dataset_path, 'r', encoding='utf-8') as file:
            tmp_data = json.load(file)
        self.data = {}
        self.roles = list(tmp_data.keys())
        for role in self.roles:
            tmp_history = []
            history = tmp_data[role]['history']
            for date in list(history.keys()):
                for qa in history[date]:
                    tmp_history.append((qa['query'], qa['response'], date))
            self.data[role] = tmp_history

    def get_next(self):
        role = self.roles.pop(0)
        return role, self.data[role]

    def get(self, role):
        return self.data[role]

    def get_size(self):
        return len(self.roles)

    def has_next(self):
        if len(self.roles) > 0:
            return True
        return False

    # 以大模型对话请求的json格式返回
    def get_next_by_request_format(self):
        role, data = self.get_next()
        messages = []
        for query, response, date in data:
            messages.append({"role": "user", "content": f'{query} 时间:{date}'})
            messages.append({"role": "assistant", "content": f'{response} 时间:{date}'})
        return role, messages

# GVD测试集
class GVDProcessedTestDataLoader(BaseDataLoader):
    def __init__(self, dataset):
        self.roles = []
        super().__init__(dataset)

    def load_data(self):
        self.data = {}
        with open(self.dataset_path, 'r', encoding='utf-8') as file:
            self.data = json.load(file)
            self.roles = list(self.data.keys())

    def get_next(self):
        role = self.roles.pop(0)
        return role, self.data[role]

    def get(self, role):
        return self.data[role]

    def get_size(self):
        return len(self.roles)

    def has_next(self):
        if len(self.roles) > 0:
            return True
        return False

if __name__ == '__main__':
    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 训练集数据展示 >>>>>>>>>>>>>>>>>>>>>>>>>")
    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    # 获取训练集数据
    train_dataset_cn = GVDTrainDataLoader(dataset='data/original/memory_bank_cn.json')
    train_dataset_en = GVDTrainDataLoader(dataset='data/original/memory_bank_en.json')
    train_dataset_cn.load_data()
    train_dataset_en.load_data()

    # 打印数据
    while train_dataset_cn.has_next():
        print(train_dataset_cn.get_next())
    while train_dataset_en.has_next():
        print(train_dataset_en.get_next())

    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 测试集数据展示 >>>>>>>>>>>>>>>>>>>>>>>>>")
    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    # 获取测试集数据
    test_dataset_cn = GVDProcessedTestDataLoader(dataset='data/processed/processed_probing_questions_cn.jsonl')
    test_dataset_en = GVDProcessedTestDataLoader(dataset='data/processed/processed_probing_questions_en.jsonl')
    test_dataset_cn.load_data()
    test_dataset_en.load_data()

    # 打印数据
    while test_dataset_cn.has_next():
        print(test_dataset_cn.get_next())
    while test_dataset_en.has_next():
        print(test_dataset_en.get_next())
