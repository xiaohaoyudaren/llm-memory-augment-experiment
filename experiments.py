import json

from dataloader import GVDTrainDataLoader, GVDProcessedTestDataLoader
from methods import BaseMethod, VecctorDBBasedChatGPTMethod
from tqdm import tqdm

class Experiment:
    def __init__(self, method:BaseMethod, experiment_name, lang='cn', overwrite=True):
        self.method = method
        self.experiment_name = experiment_name
        self.lang = lang
        self.train_dataset = None
        self.test_dataset = None
        self.results = {}
        self.base_path = 'C:/Users/50422/Desktop/论文/代码/llm-memory-augment-experiment/data/results'
        self.overwrite = overwrite
        self.results_file = f"{self.base_path}/{self.experiment_name}.json"

    # 加载数据集
    def load_data_set(self):
        # 获取训练集数据
        self.train_dataset = GVDTrainDataLoader(dataset=f'data/original/memory_bank_{self.lang}.json')
        self.train_dataset.load_data()
        # 获取测试集数据
        self.test_dataset = GVDProcessedTestDataLoader(dataset=f'data/processed/processed_probing_questions_{self.lang}.jsonl')
        self.test_dataset.load_data()

    # 执行实验
    def execute(self):
        # 1.加载数据集
        self.load_data_set()

        if not self.overwrite:
            with open(self.results_file, 'r', encoding='utf-8') as file:
                self.results = json.load(file)

        for i in tqdm(range(self.train_dataset.get_size()), desc="role"):
            # 2.初始化记忆/历史
            role, history = self.train_dataset.get_next()

            self.method.init_history(history)

            # 3.响应测试
            questions = self.test_dataset.get(role)
            for question_data in tqdm(questions, desc="提问问题"):
                if not self.overwrite and self.results.get(role) is not None:
                    has = False
                    for tmp_data in self.results.get(role):
                        if tmp_data.get("query") == question_data["question"]:
                            has = True
                            break
                    if has:
                        continue

                memory = self.method.get_memory(question_data["question"])
                response = self.method.get_response(question_data["question"])
                self.write_result(role, question_data["question"], response, memory, question_data["answer"], question_data["reference"])


    # 写入文档
    def write_result(self, role, question, response, memory, answer, reference):
        result = self.results.get(role)

        if result is None:
            self.results[role] = []
            result = self.results.get(role)

        # 持久化
        result.append({"query": question, "response": response, "memory": memory,
                       "answer": answer, "reference": reference,
                       "retrival_score": -1, "response_score": -1, "contextual_coherence": -1})
        with open(self.results_file, 'w',
                  encoding='utf-8') as json_file:
            json.dump(self.results, json_file, indent=2, ensure_ascii=False)

if __name__ == '__main__':
    # # top-1 vectordb-1 based chatgpt cn
    # experiment_name = 'vectordb-1-based-chatgpt-top-1-cn'
    # method = VecctorDBBasedChatGPTMethod(top_k=1, type=1)
    # experiment = Experiment(method=method, experiment_name=experiment_name, lang='cn')
    # experiment.execute()


    # # top-3 vectordb-1 based chatgpt cn
    # experiment_name = 'vectordb-1-based-chatgpt-top-3-cn'
    # method = VecctorDBBasedChatGPTMethod(top_k=3, type=1)
    # experiment = Experiment(method=method, experiment_name=experiment_name, lang='cn')
    # experiment.execute()
    #
    # # # top-1 vectordb-2 based chatgpt cn
    # experiment_name = 'vectordb-2-based-chatgpt-top-1-cn'
    # method = VecctorDBBasedChatGPTMethod(top_k=1, type=2)
    # experiment = Experiment(method=method, experiment_name=experiment_name, lang='cn', overwrite=False)
    # experiment.execute()

    # top-3 vectordb-2 based chatgpt cn
    # experiment_name = 'vectordb-2-based-chatgpt-top-3-cn'
    # method = VecctorDBBasedChatGPTMethod(top_k=3, type=2)
    # experiment = Experiment(method=method, experiment_name=experiment_name, lang='cn', overwrite=False)
    # experiment.execute()

    # top-3 vectordb-2 based chatgpt cn
    experiment_name = 'vectordb-1-based-chatgpt-top-3-en-bge-large-en'
    method = VecctorDBBasedChatGPTMethod(top_k=3, type=1)
    experiment = Experiment(method=method, experiment_name=experiment_name, lang='en', overwrite=False)
    experiment.execute()