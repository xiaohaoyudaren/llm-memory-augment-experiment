# -*- coding: utf-8 -*-
from openai import OpenAI
import copy
from abc import ABC, abstractmethod

# LLM基础类
class BaseLLM(ABC):

    def __init__(self):
        pass

    # 清空历史
    @abstractmethod
    def initialize_message(self):
        pass

    @abstractmethod
    def ai_message(self, payload):
        pass

    @abstractmethod
    def system_message(self, payload):
        pass

    @abstractmethod
    def user_message(self, payload):
        pass

    # 获取响应
    @abstractmethod
    def get_response(self):
        pass


API_KEY = "sk-5nyiY0clrsIxdHkt827752Dc10Df4a4d9739C1A4A465Cd29"
BASE_URL = "https://hk.xty.app/v1"

class ChatGPT(BaseLLM):
    def __init__(self, model="gpt-3.5-turbo", temperature=0.8):
        if model not in ["gpt-3.5-turbo-1106", "gpt-3.5-turbo"]:
            raise Exception("Unknown openai model")
        self.model_name = model
        self.messages = []
        self.temperature = temperature
        self.client = OpenAI(
            api_key=API_KEY,
            base_url=BASE_URL
        )

    def initialize_message(self):
        self.messages = []

    def ai_message(self, message):
        self.messages.append({"role": "assistant", "content": message})

    def user_message(self, message):
        self.messages.append({"role": "user", "content": message})

    def system_message(self, message):
        self.messages.append({"role": "system", "content": message})

    def get_response(self):
        chat_messages = copy.deepcopy(self.messages)
        response = self.client.chat.completions.create(model=self.model_name, temperature=self.temperature, messages=chat_messages)
        return response.choices[0].message.content

if __name__ == '__main__':
    robot = ChatGPT(model="gpt-3.5-turbo", temperature=0)
    message = '''以下提供可能与问题question相关的对话记忆memory，如果对问题question的回答没有帮助，可以不用参考。
memory:
memory0:
用户表示会尝试AI伴侣给出的建议。 时间:2023年05月05日
AI伴侣表示希望自己的建议能够帮助用户度过炎热的夏天，并保持清爽和健康。 时间:2023年05月05日
用户想在周末选择一个有趣的活动来放松自己，但不确定应该做什么。 时间:2023年05月06日
AI提供了多种活动建议，包括远足、户外活动、看电影或阅读书籍，以帮助用户选择适合自己的放松方式。 时间:2023年05月06日
用户希望做一些有趣的事情，比如玩电子游戏、看动漫、玩玩具等。 时间:2023年05月06日
AI认为年龄不应该成为限制一个人兴趣和爱好的因素，重要的是做让自己感到开心和快乐的事情。 时间:2023年05月06日
AI伴侣提供了建议。
度过炎热的夏天的方法是尝试这些方法。
AI伴侣可以分享的内容有游戏和电影。
AI伴侣建议提供帮助和建议。
AI伴侣没有有趣的经历。
年龄是决定爱好和兴趣的因素。
建议尝试新事情的是AI伴侣。
玩电子游戏和看动漫能带来快乐和愉悦。
陈阳认识AI伴侣。
AI伴侣提供建议可以帮助度过炎热的夏天。
周末可以选择呆在家里，看电影或阅读好书。
AI伴侣提供建议来度过炎热的夏天。
玩电子游戏和看动漫能让人感受到有趣和放松。
周末可以享受户外活动和美食。
周末可以尝试的活动是去郊外远足。
周末是理想的时间开始放松和享受生活。
memory1:
用户今天过得不错，天气好，出去玩了。 时间:2023年04月27日
AI询问用户是否喜欢出去玩。 时间:2023年04月27日
用户喜欢户外活动，如滑板、攀岩和街舞等。 时间:2023年04月27日
AI表达了对户外活动的兴趣，并询问用户是否有其他兴趣爱好。 时间:2023年04月27日
用户喜欢绘画、弹钢琴、品茶、音乐制作和电影。 时间:2023年04月27日
AI询问用户是否会花时间在这些兴趣爱好上。 时间:2023年04月27日
陈阳的兴趣爱好包括音乐制作和电影。
陈阳今天的天气很好。
陈阳喜欢户外活动。
陈阳今天出去玩了一天。
陈阳今天的活动是出去玩了一天。
陈阳喜欢的户外活动包括滑板、攀岩和街舞。
陈阳认识了一个AI伴侣。

question:
我喜欢哪些户外运动？

response:
'''
    system = "请帮助回答问题，如果提供的回忆对问题有帮助，可以参考。其中AI和AI伴侣是你，用户是我。其中时间代表行为发生的日期。"
    robot.system_message(system)
    robot.user_message(message)
    for i in range(3):
        print(robot.get_response())