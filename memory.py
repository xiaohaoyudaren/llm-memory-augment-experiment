from abc import ABC, abstractmethod
from prompts import SUMMARY_PROMPT, ACTION_EXTRACT_PROMPT, KNOWLEDGE_EXTRACT_PROMPT
from llm import ChatGPT
import requests
import json
import numpy as np
import time
import faiss
from sklearn.metrics.pairwise import cosine_similarity
from py2neo import Graph, Node, Relationship, NodeMatcher, RelationshipMatcher
import re
import os

class BaseMemory(ABC):
    # 新增记忆
    @abstractmethod
    def add_memory(self, query, response, date=None):
        pass

    # 获取记忆
    @abstractmethod
    def get_memory(self, query):
        pass

class SummaryBasedMemory(BaseMemory):
    def __init__(self):
        self.memory = ""
        self.SUMMARY_PROMPT = SUMMARY_PROMPT
        self.llm = ChatGPT(model="gpt-3.5-turbo", temperature=0)

    def add_memory(self, query, response, date=None):
        self.memory = self.get_new_summary(query, response, date)

    def get_new_summary(self, query, response, date=None):
        # 初始化prompt
        if date is not None:
            query = f'{query} (query date: {date})'
            response = f'{response} (response date: {date})'
        prv_summary = self.memory
        new_lines = f"Human: {query}\nAI: {response}"
        prompt = self.SUMMARY_PROMPT.format(summary=prv_summary, new_lines=new_lines)

        # 获取新的prompt
        self.llm.initialize_message()
        # self.llm.system_message("帮助归纳历史对话，使用中文输出。")
        self.llm.system_message("Help summarize past conversations. but not too long.")
        # self.llm.system_message("Help summarize past conversations.The query date represents the human's query date. "
        #                         "The response date represents the response date of AI.")
        self.llm.user_message(prompt)
        return self.llm.get_response()

    def get_memory(self, query):
        return self.memory


class VectorDBBasedMemory(BaseMemory):
    def __init__(self, top_k=3, type = 1):
        self.memory = VectorDB(model='bge_large_en',k=top_k)
        self.history = []
        self.type = type

    def initialize(self):
        self.memory.initialize()
        self.history = []

    def add_memory(self, query, response, date=None):
        query = f'Human:{query}'
        response = f'AI:{response}'

        if date is not None:
            query = f'{query} (query date: {date})'
            response = f'{response} (response date: {date})'

        # 保存对话历史
        self.history.append(query)
        self.history.append(response)

        # 放入向量库
        self.add_to_vectordb(query, response)

    # 放入向量库
    def add_to_vectordb(self, query, response):

        # (1)将问答分开存储
        if self.type == 1:
            self.memory.add_text(query)
            self.memory.add_text(response)

        # (2)将问答合并存储
        if self.type == 2:
            # print(f'{query}\n{response}')
            self.memory.add_text(f'{query}\n{response}')

    # 从向量库获取
    def get_from_vectordb(self, query):
        retrieved_texts = self.memory.search(query)
        texts = []

        for text, index, distance in retrieved_texts:
            # 对应方法(1)
            if self.type == 1:
                texts.append(self.history[index // 2 * 2])
                texts.append(self.history[index // 2 * 2 + 1])

            # 对应方法(2)
            if self.type == 2:
                texts.append(self.history[index * 2])
                texts.append(self.history[index * 2 + 1])

        return texts

    def get_memory(self, query):
        return self.get_from_vectordb(query)

# 用于获取百度的embedding
class BaiduEmbeding:
    def __init__(self, model='embedding-v1'):
        self.model_name = model
        self.API_KEY = "fTGGBq2ffPXQk4m37fqMcMta"
        self.SECRET_KEY = "nNlOOhjWcVquG2D8hkYQUiO4AsvG1QfX"
        self.url = f'https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/embeddings/{self.model_name}?access_token=' + self.get_access_token()


    def get_access_token(self):
        """
        使用 AK，SK 生成鉴权签名（Access Token）
        :return: access_token，或是None(如果错误)
        """
        url = "https://aip.baidubce.com/oauth/2.0/token"
        params = {"grant_type": "client_credentials", "client_id": self.API_KEY, "client_secret": self.SECRET_KEY}
        return str(requests.post(url, params=params).json().get("access_token"))

    def get_embeding(self, sentence):
        payload = json.dumps({
            "input": [
                sentence
            ]
        })
        headers = {
            'Content-Type': 'application/json'
        }
        response = requests.request("POST", self.url, headers=headers, data=payload)
        embeding = json.loads(response.text)['data'][0]['embedding']
        embeding = np.array(embeding).reshape(1, len(embeding))
        return embeding

# 用于计算句子相似度和获取句子的embedding
class SentenceUtil:
    def __init__(self, model='embedding-v1'):
        self.model_name = model
        if model == 'embedding-v1':
            self.model = BaiduEmbeding(model=self.model_name)
        elif model == 'bge_large_en':
            self.model = BaiduEmbeding(model=self.model_name)

    # 获取文本对应的句子嵌入
    def get_embedding(self, sentence):
        if self.model_name == 'embedding-v1':
            time.sleep(0.2)
            embedding = self.model.get_embeding(sentence)
        elif self.model_name == 'bge_large_en':
            embedding = self.model.get_embeding(sentence)
        else:
            time.sleep(0.2)
            embedding = self.model.get_embeding(sentence)

        return embedding

    # 获取两个句子的相似度
    def get_similarity(self, sentence1, sentence2):
        embedding1 = self.get_embedding(sentence1)
        embedding2 = self.get_embedding(sentence2)
        # 计算余弦相似度
        similarity = cosine_similarity(embedding1, embedding2)[0][0]
        return similarity

    def query_text(self, query, texts):
        similarity = []
        for text in texts:
            similarity.append((text, self.get_similarity(query, text)))
        similarity = sorted(similarity, key=lambda similarity: similarity[1], reverse=True)
        return similarity[0:3]

# 向量库
class VectorDB(object):
    def __init__(self, model='embedding-v1', k=3):
        self.model_name = model
        self.model = SentenceUtil(model=self.model_name)
        # 存储被向量化的句子
        self.texts = []
        # 每次检索向量的数量
        self.k = k

        if self.model_name == 'embedding-v1':

            self.index = faiss.IndexFlatL2(384)
        elif self.model_name == 'bge_large_en':
            # 向量索引
            self.index = faiss.IndexFlatL2(384)

    def initialize(self):
        self.texts = []
        if self.model_name == 'embedding-v1':
            self.index = faiss.IndexFlatL2(384)
        elif self.model_name == 'bge_large_en':
            # 向量索引
            self.index = faiss.IndexFlatL2(1024)

    # 添加文本，并建立向量索引
    def add_text(self, text):
        # 保证文本唯一性
        tmp = set(self.texts)
        tmp.add(text)
        if len(tmp) == len(self.texts):
            return
        # if len(text) > 990:
        #     text = text[0:990]
        # 获取文本对应的句子嵌入
        embedding = self.__get_embedding(text)

        # 加入索引并存储文本
        self.index.add(embedding)
        self.texts.append(text)

    # 批量添加文本，并建立向量索引
    def add_texts(self, texts):
        for text in texts:
           self.add_text(text)

    # 取出最相似的文本[(文本,下标,距离),...]
    def search(self, text):
        # 获取文本对应的句子嵌入
        embedding = self.__get_embedding(text)
        # 向量检索
        distances, indices = self.index.search(embedding, self.k)
        # 获取检索到的句子
        retrieved_text = []
        for index, i in enumerate(indices[0]):
            if i < 0:
                break
            retrieved_text.append((self.texts[i], i, distances[0][index]))
        return retrieved_text

    # 获取文本对应的句子嵌入
    def __get_embedding(self, text):
        count = 3
        while count > 0:
            try:
                return self.model.get_embedding(text)
            except Exception as e:
                count = count-1

# 图数据库
class GraphDB(object):
    def __init__(self):
        self.graph = Graph("http://localhost:7474/", auth=("neo4j", "123456789"), name='neo4j')

    def add_nodes_and_relation(self, er):
        e1, r, e2 = er
        node1 = self.add_node("node", name=e1)
        node2 = self.add_node("node", name=e2)
        self.add_relation(node1, r, node2)

    # 新增图节点
    def add_node(self, label, **properties):
        existing_nodes = self.graph.nodes.match(label, **properties)
        if not existing_nodes:
            new_node = Node(label, **properties)
            self.graph.create(new_node)
            return new_node
        else:
            return existing_nodes.first()

    def add_relation(self, start_node, relationship_type, end_node):
        existing_relationships = self.graph.match(nodes=(start_node, end_node), r_type=relationship_type)
        if not existing_relationships:
            new_relationship = Relationship(start_node, relationship_type, end_node)
            self.graph.create(new_relationship)
            return new_relationship
        else:
            return existing_relationships.first()

    # 清空图数据库
    def clearMemory(self):
        # 使用delete_all()方法删除所有节点和关系
        self.graph.delete_all()

    # 获取单跳内所有关系
    def get_single_hop_relationships(self, entity_name):
        # 构建Cypher查询语句
        cypher_query = (
            f"MATCH (target_entity)-[r]->(related_entity) "
            f"WHERE target_entity.name = '{entity_name}' or related_entity.name= '{entity_name}'"
            "RETURN target_entity, type(r), related_entity"
        )

        # 执行查询
        result = self.graph.run(cypher_query)
        # 处理查询结果
        relationships = []
        for record in result:
            target_entity = record["target_entity"]
            relationship = record["type(r)"]
            related_entity = record["related_entity"]

            relationships.append((target_entity['name'], relationship, related_entity['name']))

        return relationships

# 行为抽取器，从对话中抽取用户和ai行为
class ActionExtractor(object):
    def __init__(self):
        self.ACTION_EXTRACT_PROMPT = ACTION_EXTRACT_PROMPT
        self.llm = ChatGPT(model="gpt-3.5-turbo", temperature=0)

    # history = [(q1,e1),(q2,r2),...]
    def extract(self, query, response, history):
        # 生成prompt
        tmp_history = []
        for q, r in history:
            tmp_history.append(f"Human:{q}\nAI:{r}")
        tmp_history = "\n".join(tmp_history)
        prompt = self.ACTION_EXTRACT_PROMPT.format(query=query, response=response, history=tmp_history)

        system = "根据要求的格式生成用户行为和AI行为，注意保留提到的细节和举出的例子。"

        self.llm.initialize_message()
        self.llm.system_message(system)
        self.llm.user_message(prompt)
        count = 3
        action = ""
        while (count > 0):
            try:
                action = self.llm.get_response()
                result = action.split("\n")
                print(result)
                return (result[0].split(":")[1], result[1].split(":")[1])
            except Exception as e:
                count = count - 1
                if count == 0:
                    return (f'prompt:{prompt}',f'prompt:{prompt}')
                time.sleep(1)

# 知识抽取器，从对话中抽取知识
class KnowledgeExtractor(object):
    def __init__(self):
        self.KNOWLEDGE_EXTRACT_PROMPT = KNOWLEDGE_EXTRACT_PROMPT
        self.llm = ChatGPT(model="gpt-3.5-turbo", temperature=0)

    # history = [(q1,e1),(q2,r2),...]
    def extract(self, query, response, history):
        # 生成prompt
        tmp_history = []
        for q, r in history:
            tmp_history.append(f"Human:{q}\nAI:{r}")
        tmp_history = "\n".join(tmp_history)
        prompt = self.KNOWLEDGE_EXTRACT_PROMPT.format(query=query, response=response, history=tmp_history)

        system = "根据要求的格式生成知识三元组，注意不需要抽取行为信息。"

        self.llm.initialize_message()
        self.llm.system_message(system)
        self.llm.user_message(prompt)
        count = 3
        while (count > 0):
            try:
                knowledge = self.llm.get_response()
                knowledge = knowledge.replace("'", "")
                knowledge = self.extract_triplets(knowledge)
                return knowledge
            except Exception as e:
                count = count - 1
                if count == 0:
                    return (f'prompt:{prompt}', f'prompt:{prompt}')
                time.sleep(1)

    def extract_triplets(self, text):
        # 使用正则表达式匹配三元组
        pattern = r'\(([^,]+),([^,]+),([^)]+)\)'
        matches = re.findall(pattern, text)

        # 构造三元组列表
        triplets = [(match[0].strip(), match[1].strip(), match[2].strip()) for match in matches]

        return triplets

if __name__ == '__main__':
    # 知识抽取器测试
    history = [
        ("你好，朋友。今天是周末，我想放松一下，你有什么好的建议？",
         "你好，郝明。周末是放松和休息的好机会。你可以选择一些自己喜欢的活动，例如运动、阅读、看电影、旅游等。另外，也可以和朋友或家人一起聚会或旅行，享受美好的时光。")
    ]
    query = "很有道理。我最近闲暇时间都在看书，你有没有一些书推荐？"
    response = "当然。推荐一些经典的好书，例如《1984》、《人类简史》、《黑客与画家》、《心理学与生活》等。这些书涵盖了不同领域的知识和思想，有助于开阔我们的眼界和思维。"
    knowledge_extractor = KnowledgeExtractor()
    print(knowledge_extractor.extract(query, response, history))

    # # 行为抽取器测试
    # history = [
    #         ("你好，朋友。今天是周末，我想放松一下，你有什么好的建议？", "你好，郝明。周末是放松和休息的好机会。你可以选择一些自己喜欢的活动，例如运动、阅读、看电影、旅游等。另外，也可以和朋友或家人一起聚会或旅行，享受美好的时光。")
    #     ]
    # query = "很有道理。我最近闲暇时间都在看书，你有没有一些书推荐？"
    # response = "当然。推荐一些经典的好书，例如《1984》、《人类简史》、《黑客与画家》、《心理学与生活》等。这些书涵盖了不同领域的知识和思想，有助于开阔我们的眼界和思维。"
    # action_extractor = ActionExtractor()
    # print(action_extractor.extract(query, response, history))

    # # 图数据库测试
    # graph = GraphDB()
    # graph.clearMemory()
    # graph.add_nodes_and_relation(("you", "are", "stupid"))
    # print(graph.get_single_hop_relationships("you"))
    # print(graph.get_single_hop_relationships("stupid"))

    # # VectorDBBasedMemory测试
    # base_memory = VectorDBBasedMemory()
    # history = [
    #     ("你好，我叫张曼婷，很高兴认识你。", "你好，张曼婷，我是你的AI伴侣，有什么能帮到你的吗？", "2023-04-27"),
    #     ("我很喜欢绘画、弹钢琴和品茶，你觉得有什么好的书可以推荐给我吗？",
    #      "当然了，有很多关于绘画技法、钢琴演奏和品茶知识的书籍可以推荐给你。你喜欢具体的技法指导还是更偏向文艺作品?",
    #      "2023-04-27"),
    #     ("文艺作品吧，像《了不起的盖茨比》这种充满情调的。",
    #      "那我推荐《小王子》和《傲慢与偏见》，这两本书都非常有趣并且充满了情感。", "2023-04-27")
    # ]
    #
    # for line in history:
    #     base_memory.add_memory(line[0], line[1], line[2])
    #     print(base_memory.get_memory('我喜欢什么'))


    # # SummaryBasedMemory测试
    # base_memory = SummaryBasedMemory()
    # history = [
    #     ("你好，我叫张曼婷，很高兴认识你。", "你好，张曼婷，我是你的AI伴侣，有什么能帮到你的吗？", "2023-04-27"),
    #     ("我很喜欢绘画、弹钢琴和品茶，你觉得有什么好的书可以推荐给我吗？", "当然了，有很多关于绘画技法、钢琴演奏和品茶知识的书籍可以推荐给你。你喜欢具体的技法指导还是更偏向文艺作品?", "2023-04-27"),
    #     ("文艺作品吧，像《了不起的盖茨比》这种充满情调的。", "那我推荐《小王子》和《傲慢与偏见》，这两本书都非常有趣并且充满了情感。", "2023-04-27")
    # ]
    #
    # for line in history:
    #     base_memory.add_memory(line[0], line[1], line[2])
    #     print(base_memory.get_memory(line[0]))
    pass

