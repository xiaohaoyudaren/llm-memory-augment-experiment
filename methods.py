from llm import ChatGPT
from abc import ABC, abstractmethod
from memory import *
from prompts import SUMMARY_CHAT_PROMPT, DEFAULT_CHAT_PROMPT
from dataloader import GVDTrainDataLoader, GVDProcessedTestDataLoader
from tqdm import tqdm

class BaseMethod(ABC):
    # 初始化历史
    # [
    #   (q1, r1, date),
    #   (q2, r2, date),
    #   ...
    # ]
    @abstractmethod
    def init_history(self, history_list):
        pass

    # 获取响应
    @abstractmethod
    def get_response(self, query):
        pass

    # 获取记忆
    @abstractmethod
    def get_memory(self, query):
        pass

class SummaryBasedChatGPTMethod(BaseMethod):
    def __init__(self):
        self.chat = ChatGPT(model="gpt-3.5-turbo", temperature=0)
        self.base_memory = SummaryBasedMemory()
        self.CHAT_PROMPT = SUMMARY_CHAT_PROMPT


    def init_history(self, history_list):
        for history in tqdm(history_list, desc="初始化历史"):
            self.base_memory.add_memory(query=history[0], response=history[1], date=history[2])
            # self.base_memory.add_memory(query=history[0], response=history[1])
            print(self.base_memory.get_memory(''))

    def get_memory(self, query):
        return self.base_memory.get_memory(query)

    # 获取响应
    def get_response(self, query):
        memory = self.base_memory.get_memory(query)
        query_prompt = self.CHAT_PROMPT.format(history=memory, input=query)
        self.chat.initialize_message()
        self.chat.user_message(query_prompt)
        return self.chat.get_response()

class VecctorDBBasedChatGPTMethod(BaseMethod):
    def __init__(self, top_k=3, type=1):
        self.chat = ChatGPT(model="gpt-3.5-turbo", temperature=0)
        self.base_memory = VectorDBBasedMemory(top_k=top_k, type=type)
        self.CHAT_PROMPT = DEFAULT_CHAT_PROMPT


    def init_history(self, history_list):
        self.base_memory.initialize()
        for history in tqdm(history_list, desc="初始化历史"):
            self.base_memory.add_memory(query=history[0], response=history[1], date=history[2])
            # self.base_memory.add_memory(query=history[0], response=history[1])
            # print(self.base_memory.get_memory(''))

    def get_memory(self, query):
        memory = self.base_memory.get_memory(query)
        return '\n'.join(memory)


    # 获取响应
    def get_response(self, query):
        memory = self.base_memory.get_memory(query)
        # print(f'memory = {memory}\n')

        self.chat.initialize_message()

        # (1)把记忆放在prompt中
        query_prompt = self.CHAT_PROMPT.format(history='\n'.join(memory), input=query)
        # print(query_prompt)
        self.chat.user_message(query_prompt)
        return self.chat.get_response()

        # (2)把记忆放在对话历史中
        # for i in range(len(memory)//2):
        #     self.chat.user_message(memory[i * 2])
        #     self.chat.ai_message(memory[i * 2 + 1])
        # self.chat.user_message(query)
        # return self.chat.get_response()

if __name__ == '__main__':
    # 获取训练集数据
    train_dataset_cn = GVDTrainDataLoader(dataset='data/original/memory_bank_en.json')
    train_dataset_cn.load_data()
    test_dataset_cn = GVDProcessedTestDataLoader(dataset='data/processed/processed_probing_questions_en.jsonl')
    test_dataset_cn.load_data()

    # role, history = train_dataset_cn.get_next()
    role = "John Zhang"
    history = train_dataset_cn.get(role)

    print(f'>>>>>>>>>>>>>>>{role}>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
    # 1.测试基于Summary的方法
    # based_method = SummaryBasedChatGPTMethod()

    # 2.测试基于VectorDB的方法
    based_method = VecctorDBBasedChatGPTMethod(type=1)

    based_method.init_history(history)
    questions = test_dataset_cn.get(role)
    for question_data in tqdm(questions, desc="提问问题"):
        print(f'>>>>>>>>>>>>>>>question:{question_data["question"]}>>>>>>>>>>>>>>>>')
        print(based_method.get_response(question_data["question"]))

