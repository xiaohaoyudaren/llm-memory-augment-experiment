# 摘要总结prompt
SUMMARY_PROMPT = """Progressively summarize the lines of conversation provided, adding onto the previous summary returning a new summary.

EXAMPLE
Current summary:
The human asks what the AI thinks of artificial intelligence. The AI thinks artificial intelligence is a force for good.

New lines of conversation:
Human: Why do you think artificial intelligence is a force for good?
AI: Because artificial intelligence will help humans reach their full potential.

New summary:
The human asks what the AI thinks of artificial intelligence. The AI thinks artificial intelligence is a force for good because it will help humans reach their full potential.
END OF EXAMPLE

Current summary:
{summary}

New lines of conversation:
{new_lines}

New summary:"""
# 将摘要作为memory的prompt
SUMMARY_CHAT_PROMPT = """The following is a friendly conversation between a human and an AI. The AI is talkative and provides lots of specific details from its context. If the AI does not know the answer to a question, it truthfully says it does not know.

Current conversation:
{history}
Human: {input}
AI:"""
# 默认对话的prompt(带历史记忆)
DEFAULT_CHAT_PROMPT = '''The following provides dialogue memory memories that may be related to the question QUESTION.
If the answer to the question QUESTION is not helpful, there is no need to refer to it.
MEMORY:
{history}

QUESTION:
{input}

RESPONSE:
'''
# 行为抽取prompt
ACTION_EXTRACT_PROMPT = '''将 <query> 和 <response> 分别总结成用户行为和AI行为，用来帮助在后期对话中回忆过去的记忆。
注意，对于提到的特殊名词和举的例子要进行保留，以保留细节上的信息！
其中 <query>是我的提问，<response> 是你的回答，<history>是之前对话历史，可以帮助确定<query>和<response>中提到的对象：
例1
Input:
<history>
Human:今天我去了绿禾公园散步，看到了一些非常漂亮的景色，好想和你分享。
AI:好呀，你去的是哪个公园呢？看到了什么有趣的景色？
<query>
我看到了一朵开得特别美的樱花，还有一只超级可爱的松鼠！
<response>
真棒！听你这么说，我也好想去公园散步呢！
Output:
用户行为:用户分享了在绿禾公园看到了特别美的樱花和特别美的樱花。
AI行为:AI表示赞赏，并表达了自己也想去公园散步的愿望。
例2
Input:
<history>
Human:我很喜欢绘画、弹钢琴和品茶，你觉得有什么好的书可以推荐给我吗？
AI:当然了，有很多关于绘画技法、钢琴演奏和品茶知识的书籍可以推荐给你。你喜欢具体的技法指导还是更偏向文艺作品?
<query>
文艺作品吧，像《了不起的盖茨比》这种充满情调的。
<response>
那我推荐《小王子》和《傲慢与偏见》，这两本书都非常有趣并且充满了情感。
Output:
用户行为:用户喜欢有情调的文艺作品，例如《了不起的盖茨比》。
AI行为:AI推荐了《小王子》和《傲慢与偏见》这两本书籍。
Input:
<history>
{history}
<query>
{query}
<response>
{response}
Output:
'''
# 知识抽取prompt
KNOWLEDGE_EXTRACT_PROMPT = '''从<query>和<response>中推理总结出一些与人和行为无关的客观知识信息，并提炼成三元组列表[(subject,relation,object),(subject,relation,object),...]。
其中 <query>是我的提问，<response> 是你的回答，<history>是之前对话历史，可以帮助确定<query>和<response>中提到的对象：
例1
Input:
<history>
Human:今天我去了绿禾公园散步，看到了一些非常漂亮的景色，好想和你分享。
AI:好呀，你去的是哪个公园呢？看到了什么有趣的景色？
<query>
我看到了一朵开得特别美的樱花，还有一只超级可爱的松鼠！
<response>
真棒！听你这么说，我也好想去公园散步呢！
Output:
[(绿禾公园,有,一朵开得特别美的樱花),(绿禾公园,有,一只超级可爱的松鼠)]
例2
Input:
<history>
Human:我很喜欢绘画、弹钢琴和品茶，你觉得有什么好的书可以推荐给我吗？
AI:当然了，有很多关于绘画技法、钢琴演奏和品茶知识的书籍可以推荐给你。你喜欢具体的技法指导还是更偏向文艺作品?
<query>
文艺作品吧，像《了不起的盖茨比》这种充满情调的。
<response>
那我推荐《小王子》和《傲慢与偏见》，这两本书都非常有趣并且充满了情感。
Output:
[(《了不起的盖茨比》,类别,文艺作品),(《小王子》,类别,文艺作品),(《小王子》,特点,有趣且充满情感的书),(《傲慢与偏见》,类别,文艺作品),(傲慢与偏见》,特点,有趣且充满情感的书)]
Input:
<history>
{history}
<query>
{query}
<response>
{response}
Output:
'''